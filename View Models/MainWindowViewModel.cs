﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BigPSF.Models;
using BigPSF.View_Models;
using BigPSF.Model_Access;
using BigPSF.Helper_Classes;

namespace BigPSF.View_Models
{
    public class MainWindowViewModel : WorkspaceViewModel
    {
        ReadOnlyCollection<CommandViewModel> _commands;
        ReadOnlyCollection<HostViewModel> _hosts;
        readonly CollectionRepository _CollectionRepository;
        readonly HostRepository _hostRepository;
        private RelayCommand _newCollectionCommand;

        public MainWindowViewModel()
        {
            DisplayName = "IR/C Framework";
        }

        public ReadOnlyCollection<CommandViewModel> Commands
        {
            get
            {
                if (_commands == null)
                {
                    var cmds = CreateCommands();
                    _commands = new ReadOnlyCollection<CommandViewModel>(cmds);
                }

                return _commands;
            }
        }
        /*
        public ReadOnlyCollection<HostViewModel> Hosts
        {
            get
            {
                if (_hosts == null)
                {
                    var hosts = CreateHosts();
                    _hosts = new ReadOnlyCollection<HostViewModel>(hosts);
                }

                return _hosts;
            }
        }
        */
        private List<CommandViewModel> CreateCommands()
        {
            return new List<CommandViewModel>
            {
                new CommandViewModel("Exit", CloseCommand),
                new CommandViewModel("New Collection", NewCollectionCommand)
            };
        }

        /*
        // Main Window Dashboard Commands
        */

        public ICommand NewCollectionCommand
        {
            get
            {
                if (_newCollectionCommand == null)
                {
                    _newCollectionCommand = new RelayCommand(param => OnNewCollectionCommand());
                }

                return _newCollectionCommand;
            }
        }

        private void OnNewCollectionCommand()
        {
            // TODO - Make new collection user control
        }

        /*
        private List<HostViewModel> CreateHosts()
        {
            return new List<HostViewModel>
            {
                new HostViewModel(Host.Create),
                new HostViewModel(),
                new HostViewModel(new Host()),
                new HostViewModel(new Host()),
                new HostViewModel(new Host())
            };
        }
        */
    }
}
