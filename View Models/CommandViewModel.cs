﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace BigPSF.View_Models
{
    public class CommandViewModel : ViewModelBase
    {
        public ICommand Command
        { get; private set; }

        public CommandViewModel(string displayName, ICommand command)
        {
            Command = command ?? throw new ArgumentNullException("command");
            DisplayName = displayName;
        }
    }
}
