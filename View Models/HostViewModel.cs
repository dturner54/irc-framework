﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using BigPSF.Models;
using BigPSF.Model_Access;

namespace BigPSF.View_Models
{
    public class HostViewModel : ViewModelBase
    {
        readonly Host _host;
        string _log;

        public HostViewModel(Host host)
        {
            _host = host ?? throw new ArgumentNullException("host");
            DisplayName = UniqueId.ToString();
        }

        #region Host Properties

        public string UniqueId
        {
            get
            {
                return _host.UniqueId.ToString();
            }
        }

        public string IPAddress
        {
            get
            {
                return _host.Address.ToString();
            }
        }

        #endregion
    }
}
