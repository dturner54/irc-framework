﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using BigPSF.Helper_Classes;

namespace BigPSF.View_Models
{
    public class WorkspaceViewModel : ViewModelBase
    {
        private RelayCommand _closeCommand;

        public WorkspaceViewModel()
        {
        }

        public ICommand CloseCommand
        {
            get
            {
                if (_closeCommand == null)
                {
                    _closeCommand = new RelayCommand(param => OnRequestClose()); 
                }

                return _closeCommand;
            }
        }

        public event EventHandler RequestClose;

        private void OnRequestClose()
        {
            RequestClose?.Invoke(this, EventArgs.Empty);
        }
    }
}
