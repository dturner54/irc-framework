﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BigPSF.Models;

namespace BigPSF.Model_Access
{
    public class HostRepository
    {
        List<Host> _hosts;

        protected HostRepository() {}

        public void AddHost(Host host)
        {
            if (!_hosts.Contains(host))
            {
                _hosts.Add(host);
            }
            else
            {
                throw new Exception("Attempted to add duplicate Host object to HostRepository");
            }
        }

        public void RemoveHost(Host host)
        {
            if (_hosts.Contains(host))
            {
                _hosts.Remove(host);
            }
            else
            {
                throw new Exception("Attempted to remove non-existent Host object from HostRepository");
            }
        }

        public static HostRepository Create()
        {
            return new HostRepository();
        }
    }
}
