﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Management.Automation.Runspaces;

namespace BigPSF.Models
{
    public class Plugin
    {
        public Plugin(string pluginName, ScriptBlock scriptBlock)
        {
            Name = pluginName;
            Script = scriptBlock;
            WSManConnectionInfo x = new WSManConnectionInfo();
        }

        public string Name
        { get; protected set; }

        public ScriptBlock Script
        { get; protected set; }

        
    }
}
