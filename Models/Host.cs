﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management.Automation;
using System.Net;

namespace BigPSF.Models
{
    public class Host
    {
        protected Host() {}

        public Guid UniqueId
        { get; private set; }

        public IPAddress Address
        { get; private set; }

        public PSObject Members
        { get; protected set; }

        public static Host Create(IPAddress address)
        {
            var host = new Host()
            {
                UniqueId = Guid.NewGuid(),
                Address = address
           
            };
            
            return host;
        }
    }
}
